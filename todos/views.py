from django.shortcuts import render, redirect
from .models import TodoList, TodoItem
from .form import TodoListForm, TodoItemForm


def todo_lists(request):
    todo_lists = TodoList.objects.all()
    context = {"todo_lists": todo_lists}
    return render(request, "list.html", context)


def todo_list_detail(request, id):
    todo_list = TodoList.objects.get(id=id)
    todo_items = todo_list.items.all()
    context = {"todo_list": todo_list, "todo_items": todo_items}
    return render(request, "detail.html", context)


def create_todo(request):
    form = TodoListForm()
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    context = {"form": form}
    return render(request, "create.html", context)


def edit_todo(request, id):
    todo_list = TodoList.objects.get(id=id)
    form = TodoListForm()
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")

    context = {"name": todo_list.name, "form": form}
    return render(request, "edit.html", context)


def delete_todo(request, id):
    if request.method == "POST":
        TodoList.objects.get(id=id).delete()
        return redirect("todo_list_list")
    return render(request, "delete.html")


def create_item(request):
    form = TodoItemForm()
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    context = {"form": form}
    return render(request, "create_item.html", context)


def edit_item(request, id):
    todo_item = TodoItem.objects.get(id=id)
    form = TodoItemForm()
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")

    context = {"form": form}
    return render(request, "update_item.html", context)
