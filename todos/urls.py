from django.urls import path
from .views import (
    todo_lists,
    todo_list_detail,
    create_todo,
    edit_todo,
    delete_todo,
    create_item,
    edit_item,
)

urlpatterns = [
    path("", todo_lists, name="todo_list_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", create_todo, name="create_todo"),
    path("<int:id>/edit/", edit_todo, name="todo_update"),
    path("<int:id>/delete/", delete_todo, name="delete_todo"),
    path("items/create/", create_item, name="create_item"),
    path("items/<int:id>/edit/", edit_item, name="edit_item"),
]
